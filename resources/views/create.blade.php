@extends('layouts.app')
@section('content')

    <h1>Shorten URL</h1>
    <form method="post" action="{{ url('/') }}">
        @csrf
        <div class="form-group">
            <label>กรอกชื่อ URL</label>
            <input type="text" name="long_url" class="form-control" placeholder="กรุณากรอก Url..">
        </div>
            <button type="submit" class="btn btn-info">CREATE SHORT URL</button>
        <a href="{{ url('/') }}" class="btn btn-secondary"> Go back </a>
    </form>

@endsection
