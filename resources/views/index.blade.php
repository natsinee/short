@extends('layouts.app')
@section('content')
    <form class="container">
    <h1>List</h1>
        <a href="{{ url('/new') }}" class="btn btn-info"> CREATE </a>
        <hr>
{{--แบบธรรมดา--}}
{{--    @if(count($todos)>0)--}}
{{--        @foreach($todos as $todo)--}}
{{--                <div class="container">--}}
{{--                    <p>{{$todo->created_at}}</p>--}}
{{--                    <a href="{{ url($todo->long_url) }}">--}}
{{--                        <h3 class="text-primary">{{$todo->long_url}}</h3>--}}
{{--                    </a>--}}
{{--                    <div class="input-group mb-3">--}}
{{--                        <input id="shorturl{{$todo->id}}" class="form-control" type="text"--}}
{{--                               value="http://www.short.local/t/{{$todo->short_url}}" readonly>--}}
{{--                        <div class="input-group-append">--}}
{{--                            <button class="btn btn-outline-dark" onclick="copy(this)" type="button"--}}
{{--                                    value="{{$todo->id}}" id="copyBtn">copy</button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <p>{{$todo->view}}</p>--}}
{{--                </div>--}}
{{--                <hr>--}}

{{--        @endforeach--}}
{{--    @endif--}}

        <table class="table table-sm table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col" >CREATED AT</th>
                <th scope="col" >VIEW</th>
                <th scope="col" >LONG URL</th>
                <th scope="col" >SHORT URL</th>
                <th scope="col" ></th>
            </tr>
            </thead>
            @if(count($todos)>0)
                @foreach($todos as $todo)
                    <tbody>
                    <tr class="table-light">

                        <td>{{$todo->created_at}}</td>
                        <td class="text-center">{{$todo->view}}</td>
                        <td>
                            <a href="{{ url($todo->long_url) }}"> {{$todo->long_url}}
                            </a>
                        </td>
                        <td>
                            <input id="shorturl{{$todo->id}}" class="form-control" type="text"
                                value="http://www.short.local/t/{{$todo->short_url}}" readonly>
                        </td>
                        <td>
                            <button class="btn btn-outline-dark" onclick="copy(this)" type="button"
                                    value="{{$todo->id}}" id="copyBtn">copy</button>
                        </td>
                    </tr>
                    </tbody>
                        @endforeach
                    @endif
                </table>
    </form>
            <script>
                function copy(clickedBtn) {
                var id = clickedBtn.value;
                var copyText = document.querySelector('#shorturl'+id);
                copyText.select();
                document.execCommand('copy')
                    alert('Copied '+ copyText.value);
                //
                }
            </script>
        @endsection
