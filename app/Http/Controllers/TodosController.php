<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todos = Todo::OrderBy('created_at','desc')->get();
        return view('index')->with('todos',$todos);
        //todosสร้างขึ้นมาเพื่อรับModel
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return หน้า create
        return view('create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $charset = "abcdefghijklmnopqrstuvwxyz";
        $numset = "0123456789";
        $url = "";
        for($i=0; $i<5; $i++)
        {
            $url .= $numset[rand(0,9)];
            //$url=  $url . $numset[rand(0,9)];
        }
        $url .= $charset[rand(0,strlen($charset))-1];
        $url .= $charset[rand(0,strlen($charset))-1];
        $url .= $charset[rand(0,strlen($charset))-1];
        $this -> validate($request,
            [
               'long_url'=>'required',
            ]);
        $todo = new Todo();
        $todo->long_url = $request->input('long_url');
        $todo ->short_url = $url;
        $todo->view = 0;
        $todo->save();

        return redirect('/new')->with('success','success'.' short.local/'.$url);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($short)
    {
        $todos = Todo::all();
        //เจ้าTodo มาจากTodo.php มาใส่ใน ตัวแปล $todos
        if(count($todos)>0){
            foreach ($todos as $todo){
                if($todo->short_url == $short){
                    $todo->view += 1;
                    $todo->save();
                    return view('direction')->with('longurl',$todo->long_url);
                }
            }
            return view('notfound');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
