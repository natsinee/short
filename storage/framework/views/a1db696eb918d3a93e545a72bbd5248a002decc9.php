<?php $__env->startSection('content'); ?>
    <form class="container">
    <h1>List</h1>
        <a href="<?php echo e(url('/new')); ?>" class="btn btn-primary"> CREATE </a>
    <?php if(count($todos)>0): ?>
        <?php $__currentLoopData = $todos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $todo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="container">
                    <p><?php echo e($todo->created_at); ?></p>
                    <a href="<?php echo e(url($todo->long_url)); ?>">
                        <h3 class="text-primary"><?php echo e($todo->long_url); ?></h3>
                    </a>
                    <div class="input-group mb-3">
                        <input id="shorturl<?php echo e($todo->id); ?>" class="form-control" type="text"
                               value="http://www.short.local/t/<?php echo e($todo->short_url); ?>" readonly>
                        <div class="input-group-append">
                            <button class="btn btn-outline-dark" onclick="copy(this)" type="button" value="<?php echo e($todo->id); ?>" id="copyBtn">copy</button>
                        </div>
                    </div>
                    <p><?php echo e($todo->view); ?></p>
                </div>
                <hr>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
    </form>
    <script>
        function copy(clickedBtn) {
        var id = clickedBtn.value;
        var copyText = document.querySelector('#shorturl'+id);
        copyText.select();
        document.execCommand('copy')
            alert('Copied '+ copyText.value);
        //
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\short\resources\views/index.blade.php ENDPATH**/ ?>