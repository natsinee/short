<?php $__env->startSection('content'); ?>
    <form class="container">
    <h1>List</h1>
        <a href="<?php echo e(url('/new')); ?>" class="btn btn-info"> CREATE </a>
        <hr>























        <table class="table table-sm table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col" >CREATED AT</th>
                <th scope="col" >VIEW</th>
                <th scope="col" >LONG URL</th>
                <th scope="col" >SHORT URL</th>
                <th scope="col" ></th>
            </tr>
            </thead>
            <?php if(count($todos)>0): ?>
                <?php $__currentLoopData = $todos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $todo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tbody>
                    <tr class="table-light">

                        <td><?php echo e($todo->created_at); ?></td>
                        <td class="text-center"><?php echo e($todo->view); ?></td>
                        <td>
                            <a href="<?php echo e(url($todo->long_url)); ?>"> <?php echo e($todo->long_url); ?>

                            </a>
                        </td>
                        <td>
                            <input id="shorturl<?php echo e($todo->id); ?>" class="form-control" type="text"
                                value="http://www.short.local/t/<?php echo e($todo->short_url); ?>" readonly>
                        </td>
                        <td>
                            <button class="btn btn-outline-dark" onclick="copy(this)" type="button"
                                    value="<?php echo e($todo->id); ?>" id="copyBtn">copy</button>
                        </td>
                    </tr>
                    </tbody>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </table>
    </form>
            <script>
                function copy(clickedBtn) {
                var id = clickedBtn.value;
                var copyText = document.querySelector('#shorturl'+id);
                copyText.select();
                document.execCommand('copy')
                    alert('Copied '+ copyText.value);
                //
                }
            </script>
        <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/short/resources/views/index.blade.php ENDPATH**/ ?>